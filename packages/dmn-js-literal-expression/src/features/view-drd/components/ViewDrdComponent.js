import { Component } from 'inferno';


export default class ViewDrdComponent extends Component {

  constructor(props, context) {
    super(props, context);

    const { injector } = context;

    this._eventBus = injector.get('eventBus');
    this._traslate = injector.get('customTranslate');
  }

  onClick = () => {
    this._eventBus.fire('showDrd');
  }

  render() {
    return (
      <div
        className="view-drd"
        ref={ node => this.node = node }>
        <button
          onClick={ this.onClick }
          className="view-drd-button">{this.translate('View DRD')}</button>
      </div>
    );
  }

}