export default function AnnotationHeader() {
  return (
    <th className="annotation header" rowspan="3">
      Anotação
    </th>
  );
}