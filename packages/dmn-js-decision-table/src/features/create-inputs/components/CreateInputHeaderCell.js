import { Component } from 'inferno';

import {
  inject
} from 'table-js/lib/components';

export default class CreateInputsHeaderCell extends Component {
  constructor(props, context) {
    super(props, context);

    this._translate = context.inject.get('customTranslate');

    inject(this);
  }

  onClick = (event) => {
    this.editorActions.trigger('addInput');
  }

  render() {
    return (
      <th
        className="input-cell create-inputs header actionable"
        onClick={ this.onClick }
        rowspan="3"
        title={this._translate('Add Input')}>
        {this._translate('Input')} <span
          className="add-input dmn-icon-plus action-icon"
          title={this._translate('Add Input')}
        ></span>
      </th>
    );
  }
}

CreateInputsHeaderCell.$inject = [ 'editorActions' ];